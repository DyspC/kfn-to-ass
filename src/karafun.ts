import { KfnData, KfnEvent, KfnSection, KfnStyle } from './types';
import { asyncClose, asyncOpen } from './utils';
import { KfnDirectoryFile, KfnData as KfnInnerData, IKfnFileReader, readerForFile } from 'kfn-utils';

const sectionRegex = /\[\w+\]\r?\n([^=\r\n]+=[^\r\n]*\r?\n)+/g;
const NBSP = ' ';
const instructionStart = "instrStart";
const instructionEnd = "instrEnd";

export function parseKfn(fileContent: string, encodingIn?: BufferEncoding, encodingOut?: string): KfnData {
	if(!encodingIn)
		encodingIn = "binary";
	if(!encodingOut)
		encodingOut = "utf8";
	const readableOffset: number = fileContent.indexOf('[General]');
	if(readableOffset == -1)
		throw "assert.file.hasGeneral";
	const instructions = extractInstructions(extractCommentZone(fileContent));
	let meta = {};
	try {
		if(encodingOut == 'binary')
			meta = getMetadata(fileContent, encodingOut);
	} catch (error) {
		console.error("Couldn't parse metadata for this file")
	}
	const readablePart = Buffer.from(fileContent.substring(readableOffset), encodingIn).toString(encodingOut);
	return {sections: parseSections(readablePart, instructions), metadata: meta};
}

export async function parseKfnFile(kfnPath: string, encodingOut?: string): Promise<KfnData> {
	if(!encodingOut)
		encodingOut = "utf8";

	const kfnHandle: number = await asyncOpen(kfnPath, 'r');

	const kfnReader: IKfnFileReader = readerForFile(kfnHandle);

	try {
		await kfnReader.buildDirectory();
	} catch (e) {
		await asyncClose(kfnHandle);
		throw e; // Rethrow unhandled
	}

	const kfnData: KfnInnerData = kfnReader.data;
	const instructions = extractInstructions(kfnData.headers['COMM'] as string);
	const lyricsFiles: KfnDirectoryFile[] = kfnReader.data.directory.files
		.filter((dirFile: KfnDirectoryFile) => dirFile.type == 1);
	if (!lyricsFiles || lyricsFiles.length == 0) {
		throw `Malformed input: no Song.ini files were found in ${kfnPath}`;
	}
	const readablePart = (await kfnReader.readFile(lyricsFiles[0].length1, lyricsFiles[0].offset))
		.toString(encodingOut);

	await asyncClose(kfnHandle);

	return {sections: parseSections(readablePart, instructions), metadata: kfnReader.data.headers};
}

function parseSections(content: string, instructions: any): KfnSection[] {
	const res: KfnSection[] = [];
	const general = readSection(content, "General");
	const effCount = parseInt(readValue(general, "EffectCount"));
	const disabledEffects = (instructions.global.disabled || "").split(/, */).filter(e => e.length > 0);

	for(let i = 0; i++ < effCount;){
		const effectName = `Eff${i}`;
		const effect = readSection(content, effectName);
		if(sectionIsSung(effect) && !disabledEffects.includes(effectName)){
			const section: Partial<KfnSection> = {};

			section.name = `${i}-${readValue(effect, "Caption")}`;
			section.style = extractStyle(effect, 
				instructions[effectName] || {}, 
				instructions.global)
			section.times = extractTimes(effect);
			section.lyrics = extractLyrics(effect);
			section.events = extractEvents(effect);

			res.push(section as KfnSection);
		}
	}
	return res;
}

function extractStyle(effect: string, specificInstructions: any, globalInstructions: any): KfnStyle {
	return {
		inactiveColor: specificInstructions["InactiveColor"] || globalInstructions["InactiveColor"] || readValue(effect, "InactiveColor"),
		inactiveBorderColor: specificInstructions["InactiveFrameColor"] || globalInstructions["InactiveFrameColor"] || readValue(effect, "InactiveFrameColor"),
		activeColor: specificInstructions["ActiveColor"] || globalInstructions["ActiveColor"] || readValue(effect, "ActiveColor"),
		activeBorderColor: specificInstructions["FrameColor"] || globalInstructions["FrameColor"] || readValue(effect, "FrameColor"),
		progressive: readValue(effect, "IsFill") !== "0",
		assPosition: specificInstructions["Alignment"] || globalInstructions["Alignment"] || "8",
	};
}

function extractTimes(effect: string) {
	let timeArray: number[] = [];
	let syncCount = 0;
	while (readValue(effect, `Sync${syncCount}`) !== null) {
		const values = readValue(effect, `Sync${syncCount++}`).split(",");
		timeArray = timeArray.concat(values.map(i => parseInt(i)));
	}
	return timeArray;
}

function extractLyrics(effect: string) {
	const lyrCount = parseInt(readValue(effect, "TextCount"));
	const lyrArray: string[][] = [];
	for (let lyrIndex = 0; lyrIndex < lyrCount; lyrIndex++) {
        const read = readValue(effect, `Text${lyrIndex}`);
        if(read.length > 0) {
			const line = read.split(/ +/).join(" /")
				.replace(/_/g, NBSP)
				.split("/");
            if (line.length > 0)
                lyrArray.push(line);
        }
	}
	return lyrArray;
}

function extractEvents(effect: string) {
	const evtCount = parseInt(readValue(effect, "NbAnim"));
	const evtArray: KfnEvent[] = [];
	for (let evtIndex = 0; evtIndex < evtCount; evtIndex++) {
		const eventContent = readValue(effect, `Anim${evtIndex}`).split("|");
		if (eventContent.length > 1) {
			const event: KfnEvent = { timeCs: parseInt(eventContent[0]) };
			for (let action of eventContent.slice(1)) {
				if (action.startsWith("ChgBoolIsFill"))
					event.progressive = action.endsWith("1");
				// more
			}
			evtArray.push(event);
		}
	}
	return evtArray;
}

function sectionIsSung(sectionContent: string): boolean {
	const enabledStr = readValue(sectionContent, "Enabled");
	return (enabledStr == null || enabledStr !== "0") && (readValue(sectionContent, "LineCount") !== null || readValue(sectionContent, "Trajectory") !== null);
}

function readValue(content: string, key: string): string {
	const oLines: string[] = content.split(/\r?\n/).filter(line => line.startsWith(`${key}=`));
	if(oLines.length == 0)
		return null;
	return oLines[0].substring(key.length + 1);
}

function readSection(content: string, sectionKey: string): string {
	const sections: RegExpMatchArray = content.match(sectionRegex);
	const filtered = sections.filter(text => text.startsWith(`\[${sectionKey}\]`));
	if(filtered.length !== 1)
		throw `assert.section.unicity [${sectionKey}]`;
	return filtered[0];
}

function extractCommentZone(content: string): string {
	return content.substring(content.indexOf("COMM")+4, content.indexOf("YEAR"));
}

function getMetadata(fileContent: string, encoding: string): any {
	const header = fileContent.substring(fileContent.indexOf("KFNB")+4, fileContent.indexOf("ENDH"));
	return getMetadataFromHeader(header, encoding);
}

function getMetadataFromHeader(header: string, encoding: string): any {
	const ret = {};
	let cursor = 0;
	while(cursor < header.length) {
		const key = header.substr(cursor, 4);
		if(!key.match("[A-Z]{4}")) {
			console.debug(`Bad key in header at pos ${cursor}: '${key}'`);
			cursor++; // skip
			continue;
		}
		cursor += 4;
		const dataType = header.charCodeAt(cursor++);

		let next4Bytes = 0;
		for(let i = cursor + 3; i >= cursor; i--) {
			next4Bytes = 2 * next4Bytes + header.charCodeAt(i);
		}
		cursor += 4;

		if(dataType == 1) {
			if (next4Bytes != 0) // Don't store empty fields
				ret[key] = next4Bytes; // 4 bytes contain value
		} else if(dataType == 2) {
			const temp = Buffer.from(header.substr(cursor, next4Bytes), "binary").toString(encoding)
				.replace(/\u0000+/, "").trim(); // 4 bytes contain string length and pls no \0 everywhere
			if(temp.length > 0)
				ret[key] = temp; // Don't store empty fields
			cursor += next4Bytes;
		} else {
			console.error(`ALED key=${key} dataType=${dataType} (${String.fromCharCode(dataType)})`);
			throw "karafunbad";
		}
	}
	return ret;
}

function extractInstructions(commentZone: string): any {
	const resp = {global: {}};
		if (commentZone) {
		const startingPoint = commentZone.indexOf(instructionStart);
		const endingPoint = commentZone.indexOf(instructionEnd);
		if(startingPoint > 0 && startingPoint < endingPoint) {
			const toParse = commentZone.substr(startingPoint+instructionStart.length, endingPoint);
			const toParseArray = toParse.split(/[\r\n]/).filter(e => e.length > 0).filter(e => e.includes("="));
			toParseArray.forEach(line => {
				const split = line.split('=');
				const tree = split[0].split('.');
				if (resp[tree[0]] == undefined)
					resp[tree[0]] = {};
				resp[tree[0]][tree.slice(1).join(".")] = split.slice(1).join("=");
			});
		}
	}
	return resp;
}
