	export function convertKfnToAss(kfn: KfnData, options: Options): string;
 
	export function parseKfn(fileContent: string, encodingIn?: BufferEncoding, encodingOut?: string): KfnData;
 
	export function parseKfnFile(kfnPath: string, encodingOut?: string): KfnData;

	export interface KfnData {
		sections: KfnSection[],
		metadata: object
	}
	export interface KfnSection {
		name: string,
		style: KfnStyle,
		times: number[],
		lyrics: string[][], 
		events: KfnEvent[]
	}
	export interface KfnStyle {
		inactiveColor: string,
		inactiveBorderColor: string,
		activeColor: string,
		activeBorderColor: string,
		progressive: boolean,
		// others?
		// instructions
		assPosition?: "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
	}
	export interface KfnEvent {
		timeCs: number,
		progressive?: boolean
		// others?
	}

	export interface Options {
		offset?: number,
		useFileInstructions?: boolean
	}