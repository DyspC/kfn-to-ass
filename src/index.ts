#!/usr/bin/env node
import { asyncExists, asyncReadFile, csToAss, clone, asyncWriteFile } from './utils';
import stringifyAss from 'ass-stringify';
import parseAss from 'ass-parser';
import { Command, OptionValues } from 'commander';
import { KfnData, KfnEvent, KfnSection, Options } from './types';
import { parseKfn as _parseKfn, parseKfnFile as _parseKfnFile } from './karafun';
const ass = require('./assTemplate');
const pjson = require('../package.json');

class SyncedSyllable {
	constructor(public text: string, public time: number, public progressive: boolean) {
	}
}

export function parseKfn(fileContent: string, encodingIn?: BufferEncoding, encodingOut?: string): KfnData { // Proxy Method
	return _parseKfn(fileContent, encodingIn, encodingOut);
}

export function parseKfnFile(kfnPath: string, encodingOut?: string): Promise<KfnData> { // Proxy Method
	return _parseKfnFile(kfnPath, encodingOut);
}

export function convertKfnToAss(kfn: KfnData, options: Options): string {
	const defaultStyle = ass.styles.body.filter(style => style.key == "Style")[0];
	const styles = clone(ass.styles);
	const dialogues = [];
	const offset: number = options.offset || 0;
	for(let section of kfn.sections) {
		const sectionId: string = section.name.replace(/,/g, '_');

		const sectionStyle = clone(defaultStyle);
		sectionStyle.value.Name = sectionId;
		if(options.useFileInstructions && section.style.assPosition !== undefined) {
			sectionStyle.Alignment = section.style.assPosition;
		}

		const joinedSyncs: SyncedSyllable[][] = joinSyncs(section);

		if(joinedSyncs.length > 0) {
			styles.body.push(sectionStyle);
		}

		for(let line of joinedSyncs) {
			const dialogue = clone(ass.dialogue);

			const lineStart = line[0].time + offset - ass.previewTime
			const lineEnd = line[line.length-1].time + offset + ass.postviewTime;
			const correction = lineStart < 0 ? lineStart : 0;

			dialogue.value.Style = sectionId;
			dialogue.value.Start = csToAss(lineStart - correction);
			dialogue.value.End = csToAss(lineEnd - correction);
			dialogue.value.Text = writeLineAsAss(line);

			dialogues.push(dialogue);
		}
	}

	const scriptInfo = clone(ass.scriptInfo);
	scriptInfo.body[0].value = scriptInfo.body[0].value.replace("%version", pjson.version);
	scriptInfo.body[1].value = scriptInfo.body[1].value.replace("%options", JSON.stringify(options));
	const events = clone(ass.events);
	events.body = events.body.concat(dialogues);

	return stringifyAss([scriptInfo, styles, events]);
	
}

function joinSyncs(section: KfnSection): SyncedSyllable[][] {
	const res: SyncedSyllable[][] = [];
	const currentStyle: KfnEvent = {
		timeCs: -1,
		progressive: section.style.progressive
	}
	const lyrics = clone(section.lyrics);
	const times = clone(section.times);
	const events = clone(section.events);

	let joinedLineArray: SyncedSyllable[] = [];
	let currentLine: string[] = [];
	for(let time of times) {
		if(lyrics.length == 0 && currentLine.length == 0)	// Too much times
			break;
		if(currentLine.length == 0) {	// Fetch next lyrics
			currentLine = lyrics.shift();
			if (joinedLineArray.length > 0) res.push(joinedLineArray);
			joinedLineArray = [];
		}
		while(events.length > 0 && events[0].timeCs <= time){
			updateCurrentStyle(currentStyle, events.shift());
		}
		joinedLineArray.push(new SyncedSyllable(currentLine.shift(), time, currentStyle.progressive));
	}
	if(currentLine.length > 0) {	// Complete line if too few times
		const lastProcessed = joinedLineArray.pop();
		lastProcessed.text += currentLine.join('');
		joinedLineArray.push(lastProcessed);
	}
	if(joinedLineArray.length > 0) {
		res.push(joinedLineArray);
	}
	return res;
}

function updateCurrentStyle(style: KfnEvent, event: KfnEvent) {
	if(event.progressive !== undefined) style.progressive = event.progressive;
}

function writeLineAsAss(line: SyncedSyllable[]) {
	if(line.length == 0) return "";
	const realPreview = Math.max(Math.min(ass.previewTime, line[0].time), 0);
	const resArray: string[] = [];
	resArray.push(`{\\k${realPreview}\\fad(300,200)}`);
	for(let i = 0; i < line.length-1; i++) {
		resArray.push(`{\\${line[i].progressive?"kf":"k"}${line[i+1].time-line[i].time}}${line[i].text}`);
	}
	resArray.push(`{\\${line[line.length-1].progressive?"kf":"k"}${ass.postviewTime}}${line[line.length-1].text}`);
	return resArray.join('');
}

async function mainCLI(){
	const argv: OptionValues = parseArgs();
	if(!argv.in) 
		throw "--in argument is required";

	const kfnFile: string = argv.in;
	const correctionString: string = argv.correction;
	const outFile: string = argv.out || argv.refresh;

	const correction = (correctionString && !isNaN(+correctionString))
		? +correctionString : 0;

	let refreshOptions: Partial<Options> = {};
	
	if (argv.refresh) {
		const replayAss = parseAss(await asyncReadFile(argv.refresh), {comments: true});
		const infoComments = replayAss.filter(section => section.section === 'Script Info')
			.flatMap(section => section.body.filter(info => info.type === 'comment').map(info => info.value));
		for(let i = 0; i < infoComments.length - 1; i++) {
			if (infoComments[i].startsWith('Converted using kfn-to-ass: ')
				&& infoComments[i+1].startsWith('and using options: ')) {
					refreshOptions = JSON.parse(
						infoComments[i+1].substring('and using options: '.length)) as Partial<Options>; 
						// Cast to partial options to make sure unwanted fields are not propagated further
			}
		}
	}


	const options: Options = {
		offset: (refreshOptions.offset || 0) + correction,
		useFileInstructions: refreshOptions.useFileInstructions || argv.guide
	};

	if (!await asyncExists(kfnFile))
		throw `File ${kfnFile} does not exist`;

	let kfn: KfnData;
	try {
		kfn = await _parseKfnFile(kfnFile, "utf8");
	} catch (e) {
		if (e !== 'file.bad.signature')
			throw e; // Unhandled
		// If the file is ini formatted it won't be handled by kfn-utils but can be converted through the raw binary parser
		const fileContent = await asyncReadFile(kfnFile, "binary");
		kfn = _parseKfn(fileContent, "binary", "utf8");
	}
	const assString = convertKfnToAss(kfn, options);

	if (!outFile || outFile == '-') {
		console.log(assString);
	} else {
		await asyncWriteFile(outFile, assString);
	}
}

function parseArgs(): OptionValues {
	const argv = process.argv.filter(e => e !== '--');
	return new Command('kfn-to-ass')
		.description('Converts karafun files to ass format')
		.version(pjson.version)
		.option('-i, --in <kfnFile> (required)', 'the file to read data from')
		.option('-r, --refresh <assFile>', 'replays the conversion with the parameters used to create <assFile>, and overwrites it unless specified otherwise')
		.option('-o, --out <assFile>', 'if set, saves the converted data there instead of printing it on stdout')
		.option('-c, --correction <cs>', 'offsets said amount of cs on each line', '0')
		.option('--no-guide', "ignores file instructions")
		.option('--test', "for test utils")
		.parse(argv)
		.opts();
}


if (require.main === module) mainCLI()
	.catch(err => console.error(err));

