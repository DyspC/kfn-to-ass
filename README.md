# kfn-to-ass

Conversion tool made to parse KFN files (https://www.karafun.fr) and rewrite their lyrics using ASS format

## Usage

### CLI

```bash
# Prints conversion on stdout
kfn-to-ass -i <KFN file>

# Prints conversion into specified file
kfn-to-ass -i <KFN file> -o <ASS file>

# Converts file and applies the specified offset (in cs)
kfn-to-ass -i <KFN file> -c <offset>

# Refresh an ass file written by kfn-to-ass with newer content from the KFN source
kfn-to-ass -i <KFN file> -r <ASS file>

# Use parameters from an ass file written by kfn-to-ass on content from the KFN source, make the lyrics appear 1s earlier and output on stdout instead of overwriting the ass source
kfn-to-ass -i <KFN file> -r <ASS file> -c -100 -o -

# Displays more help
kfn-to-ass --help
```

### Module

#### function parseKfn(fileContent: string, encodingIn?: BufferEncoding, encodingOut?: string): KfnData

Loads a `KfnData` structure based on said `fileContent`

`encodingIn` is the one you used to read your file (defaults as 'binary')

`encodingOut` is the one you want your output to use (defaults as 'utf8')

It is recommended you read the fileContent with a 'binary' encoding so the header can be properly decoded

#### function convertKfnToAss(kfn: KfnData, options: Options): string

Prepares said `kfn` and returns it as a `string`

#### interface Options

Wraps all options that could be used during conversion. Its members are:
- offset (number): offsets every line
- useFileInstructions (boolean): allows conversion tweaks KFN authors could have written on a per file basis
